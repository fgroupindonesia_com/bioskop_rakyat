﻿Module HistoryHelper

    Private fileName As String = "history.log"
    Private systemFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\brakyat"
    Private fileWriter As System.IO.StreamWriter
    Private fileReader As System.IO.StreamReader
    Private dURLs As New ArrayList
    Private dTitle As New ArrayList
    Private dataRead As String
    Private dataReadTemp As Array

    Public Sub prepareSystemFolder()
        If (My.Computer.FileSystem.DirectoryExists(SystemPath) <> True) Then
            My.Computer.FileSystem.CreateDirectory(SystemPath)
        End If
    End Sub

    Public Property SystemPath() As String
        Get
            Return systemFolder
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property FilePath() As String
        Get
            Return SystemPath + "\" + fileName
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Public Property TheTitles() As ArrayList
        Get
            Return dTitle
        End Get
        Set(ByVal value As ArrayList)
            dTitle = value
        End Set
    End Property

    Public Property TheURLs() As ArrayList
        Get
            Return dURLs
        End Get
        Set(ByVal value As ArrayList)
            dURLs = value
        End Set
    End Property

    Public Sub clearDataHistory()

        TheURLs.Clear()
        TheTitles.Clear()

        If (My.Computer.FileSystem.FileExists(FilePath)) Then
            My.Computer.FileSystem.DeleteFile(FilePath)
        End If

    End Sub

    Public Sub readDataHistory()

        TheURLs.Clear()
        TheTitles.Clear()

        ' only read if the file is exist
        If (My.Computer.FileSystem.FileExists(FilePath)) Then
            fileReader = New System.IO.StreamReader(FilePath)

            While (Not fileReader.EndOfStream)
                dataRead = fileReader.ReadLine
                If (dataRead IsNot Nothing) Then

                    If (dataRead.Length > 1) Then

                        dataReadTemp = dataRead.Split(";")

                        ' adding the url
                        TheURLs.Add(dataReadTemp(0))
                        ' adding the title
                        TheTitles.Add(dataReadTemp(1))

                    End If

                End If

            End While

            fileReader.Close()

        End If

        

    End Sub

    Public Sub saveDataHistory(ByVal aNameURL, ByVal aTitle)

        ' after saving into file
        TheURLs.Add(aNameURL)
        TheTitles.Add(aTitle)

        ' we will limit until 7 only 
        If (TheURLs.Count = 8) Then
            ' removing the first item
            TheURLs.RemoveAt(0)
            TheTitles.RemoveAt(0)
        End If


        fileWriter = New System.IO.StreamWriter(FilePath)

        Dim i As Integer = 0
        For Each url As String In TheURLs
            ' write out with the title
            fileWriter.WriteLine(url + ";" + TheTitles.Item(i))
            i += 1
        Next

        fileWriter.Close()



    End Sub

    Public Function hasData()
        If (TheTitles.Count > 0) Then
            Return True
        End If

        Return False
    End Function

End Module
