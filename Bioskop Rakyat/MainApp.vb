﻿Imports Gecko
Imports Gecko.JQuery

Public Class MainApp

    Dim versionApp As String = "1.2"
    Dim jQuery As JQueryExecutor

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Xpcom.EnableProfileMonitoring = False
        Xpcom.Initialize("Firefox")


        GeckoWebBrowser1.Navigate("http://lk21.org")
        jQuery = New JQueryExecutor(GeckoWebBrowser1.Window)

        HistoryHelper.prepareSystemFolder()

        ' recreate the submenu
        recreateSubMenu()

    End Sub

    Public Sub removeAds()

        If (jQuery.ExecuteJQuery("typeof jQuery == 'undefined'").ToBoolean) Then
            'MsgBox("unable using jquery")

        Else

            hideThisID("f1")
            hideThisID("f2")
            hideThisID("f3")
            hideThisID("f4")

            hideThisID("f1")
            hideThisID("f2")
            hideThisID("f3")
            hideThisID("f4")

            hideThisID("floatbtmleft")
            hideThisID("floatbtmright")
            hideThisID("ads-pop")
            hideThisID("hbanner")
            hideThisID("ptbanner")
            hideThisID("hbanner")
            hideThisID("s0")
            hideThisID("s1")
            hideThisID("s2")
            hideThisID("s3")
            hideThisID("s4")
            hideThisID("s5")
            hideThisID("s6")
            hideThisID("s7")
            hideThisID("s8")
            hideThisID("pbanner")

            disableRightClick()

            removeAdsSpan()

        End If

    End Sub

    Private Sub removeAdsSpan()
        jQuery.ExecuteJQuery("$('span').filter( function() {     if($(this).text() == 'CLOSE'){ 			$(this).hide(); 	} });")
    End Sub

    Private Sub hideThisID(ByVal aName As String)
        jQuery.ExecuteJQuery("$('#" + aName + "').hide();")
    End Sub

    Private Sub disableRightClick()

        jQuery.ExecuteJQuery("document.addEventListener('contextmenu', event => event.preventDefault());")

    End Sub


    Private Sub GeckoWebBrowser1_ProgressChanged(sender As Object, e As GeckoProgressEventArgs) Handles GeckoWebBrowser1.ProgressChanged

        If (e.CurrentProgress < e.MaximumProgress) Then
            StatusStrip1.Visible = True
            ToolStripStatusLabel1.Text = "Progress : " & e.CurrentProgress
        Else
            ToolStripStatusLabel1.Text = "Progress : Fully loaded!"
            StatusStrip1.Visible = False
        End If

        removeAds()


    End Sub

    Private Function removeUnimportantText(ByVal aTitleLong As String)
        aTitleLong = aTitleLong.Replace("Nonton ", "")
        aTitleLong = aTitleLong.Replace("Film Streaming Subtitle Indonesia Download Movie Cinema 21 Bioskop - Lk21 Layarkaca21", "")

        Return aTitleLong.Replace(" Film Streaming Download Movie Cinema 21 Bioskop Subtitle Indonesia - Lk21 Layarkaca21", "")

    End Function

    Private Sub SaveCurrentMovieToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveCurrentMovieToolStripMenuItem.Click

        Dim thisPageURL As String = GeckoWebBrowser1.Document.Url.ToString
        Dim thisPageTitle As String = GeckoWebBrowser1.Document.Title.ToString

        thisPageTitle = removeUnimportantText(thisPageTitle)

        HistoryHelper.saveDataHistory(thisPageURL, thisPageTitle)

        ' recreate the submenu
        recreateSubMenu()

    End Sub

    Private Sub recreateSubMenu()

        HistoryHelper.readDataHistory()

        If (LoadLastMovieToolStripMenuItem.DropDownItems.Count <> 0) Then
            LoadLastMovieToolStripMenuItem.DropDownItems.Clear()
        End If

        If (HistoryHelper.hasData()) Then
            For i As Integer = 0 To HistoryHelper.TheURLs.Count - 1

                Dim title As String = HistoryHelper.TheTitles.Item(i)
                Dim url As String = HistoryHelper.TheURLs.Item(i)

                Dim nNewMenu As New ToolStripMenuItem(title, Nothing, AddressOf changeTheMovie, title)
                nNewMenu.Tag = url

                LoadLastMovieToolStripMenuItem.DropDownItems.Add(nNewMenu)
            Next
        End If


        If (LoadLastMovieToolStripMenuItem.DropDownItems.Count <> 0) Then
            LoadLastMovieToolStripMenuItem.Enabled = True
            ClearHistoryToolStripMenuItem.Visible = True
        Else
            LoadLastMovieToolStripMenuItem.Enabled = False
            ClearHistoryToolStripMenuItem.Visible = False
        End If

    End Sub

    Private Sub changeTheMovie(ByVal sender As Object, ByVal e As EventArgs)

        Dim anURL As String = CType(sender, ToolStripMenuItem).Tag
        GeckoWebBrowser1.Navigate(anURL)

    End Sub


    Private Sub VersionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VersionToolStripMenuItem.Click
        MsgBox("Bioskop Rakyat v" + versionApp + " -Free Version")
    End Sub

    Private Sub ReportBugsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReportBugsToolStripMenuItem.Click
        Dim email As String = InputBox("Masukkan Email anda : ", "Bugs Report", "")

        If (email.Length > 0) Then

            Dim pesan As String = InputBox("Masukkan apa saran anda untuk kami...?", "Bugs Report", "")

            If (pesan.Length > 0) Then
                ' send email
                Using client As New System.Net.WebClient
                    Dim reqparm As New Specialized.NameValueCollection
                    reqparm.Add("pesan", pesan)
                    reqparm.Add("surel", email)
                    Dim responsebytes = client.UploadValues("http://fgroupindonesia.com/api/reports/bugs.php", "POST", reqparm)
                    Dim responsebody = (New System.Text.UTF8Encoding).GetString(responsebytes)

                    If (responsebody = 1) Then
                        MsgBox("Pesan anda telah terkirim." + vbNewLine + "Tim kami akan merespond anda dalam hitungan max 3 x 24jam." + vbNewLine + "Jika tidak ada respond, langsung saja kontak ke nomor kami di http://fgroupindonesia.com")
                    End If

                End Using



            End If
        Else
            MsgBox("Isi dulu data dengan lengkap!")
        End If

    End Sub

    Private Sub HomeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HomeToolStripMenuItem.Click
        GeckoWebBrowser1.Navigate("http://lk21.org")
    End Sub

    Private Sub StayAliveScreen_Tick(sender As Object, e As EventArgs) Handles StayAliveScreen.Tick
        SendKeys.Send("+")
    End Sub

    Private Sub MainApp_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "Bioskop Rakyat v" + versionApp
    End Sub

    Private Sub ClearHistoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClearHistoryToolStripMenuItem.Click
        HistoryHelper.clearDataHistory()

        ' call again the menu construction
        recreateSubMenu()
    End Sub

    Private Sub MenuStrip1_MouseLeave(sender As Object, e As EventArgs) Handles MenuStrip1.MouseLeave
        MenuStrip1.AutoSize = False
        MenuStrip1.Height = 12
    End Sub

    Private Sub MenuStrip1_MouseHover(sender As Object, e As EventArgs) Handles MenuStrip1.MouseHover
        MenuStrip1.Height = 25
    End Sub
End Class
