# README #

Pastikan anda menggunakan Visual Studio 2013, dengan .NET minimum v4.5. Tidak ada batasan versi windows karena aplikasi akan berjalan baik itu windows xp, 7, 8, maupun 10.

### Untuk apa Repository ini? ###

* Ringkasan
Repository ini disediakan untuk anda developers manapun yang menghendaki untuk melakukan embedding web app ke dalam desktop app. Penerapan sederhana dapat dilihat dari Bioskop App yang tersedia di dalam Repo ini.
* Versi
Adapun versi terkini ialah versi 1.2.

### Cara Setting ###

* Konfigurasi
Tidak ada tools lain yang dibutuhkan kecuali Visual Studio 2013, dan .Net versi 4.5.

* Dependencies
Tidak ada ketergantungan lain.

* Database
Tidak menggunakan database app.

* Deployment
Langsung pull Repo ini dan jalankan pada Visual Studio masing-masing.

### Petunjuk Kontribusi ###

* Gambaran Umum
Siapapun anda, developers pengguna bahasa apapun, C#, C++ / VB.net skalipun. Anda dapat bergabung dalam kontribusi code Repo ini ataupun bahkan anda dapat meniru konsepnya dan mengembangkan lebih baik lagi pada aplikasi lain. Tetapi, tentunya dan yang paling utama, anda diharapkan menyertakan diri dalam kontribusi pengembangan berikutnya.

### Kepada Siapa Saya Dapat bertanya? ###

* Jalur Komunikasi
Untuk dapat berkomunikasi kepada kami, langsung saja periksa kontak yang disediakan pada http://fgroupindonesia.com/kontak melalui alamat / kontak handphone yang tertera. Adapun majori developers menggunakan mediasi email / whatsapp untuk yang memiliki close-case. Untuk saat ini forum / online discussion dapat dilakukan di dalam facebook group halaman terkait http://facebook.com/groups/fgroupindonesia . 

Good luck everyone!